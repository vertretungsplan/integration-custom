/*
 * vertretungsplan.io static list integration
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { createHash } from 'crypto'
import LRU from 'lru-cache'
import fetch from 'node-fetch'

interface UrlInfo {
  lastModified?: number
  mimeType: string
  sha512: string
  etag?: string
  size: number
}

const cache = new LRU<string, UrlInfo>({
  max: 1000
})

export async function getUrlInfo (url: string, signal: AbortSignal) {
  const oldData = cache.get(url)
  const newData = await getUrlInfoInternal(url, signal, oldData)

  cache.set(url, newData)

  return newData
}

async function getUrlInfoInternal (url: string, signal: AbortSignal, lastResponse?: UrlInfo): Promise<UrlInfo> {
  const response = await fetch(url, {
    signal,
    headers: lastResponse && lastResponse.etag ? {
      'If-None-Match': lastResponse.etag,
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0'
    } : {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0'
    }
  })

  if (response.status === 304) {
    // not modified

    if (!lastResponse) {
      throw new Error('server reported 304 altough there is no lastResponse')
    }

    return lastResponse
  }

  if (response.status !== 200) {
    throw new Error('server reported ' + response.status)
  }

  const etag = response.headers.get('etag')
  const lastModified = Date.parse(response.headers.get('last-modified') || 'invalid')
  const mimeType = response.headers.get('content-type') || 'application/unknown'
  const body = response.body

  const hash = createHash('sha512')
  let size = 0

  // this is because typescript does not understand that this is never null
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  for await (const chunck of body!) {
    hash.update(chunck)

    size += chunck.length
  }

  return {
    sha512: hash.digest('hex'),
    lastModified: isNaN(lastModified) ? undefined : lastModified,
    mimeType,
    etag: etag || undefined,
    size
  }
}
