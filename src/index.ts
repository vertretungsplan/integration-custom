/*
 * vertretungsplan.io custom crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import express from 'express'
import { config } from './config.js'
import { elements } from './elements.js'

const app = express()

async function getStatus () {
  const institutions: Array<{
    id: string
    promise: Promise<string>
  }> = []

  elements.forEach((element) => {
    institutions.push({
      id: element.id,
      promise: element.getStatus()
        .then(() => 'OK')
        .catch(() => 'issues at ' + element.id)
    })
  })

  const resolvedPromises = await Promise.all(institutions.map((item) => item.promise))
  const filteredPromises = resolvedPromises.filter((item) => item !== 'OK')

  if (filteredPromises.length === 0) {
    if (institutions.length === 0) {
      return 'no institutions configured'
    } else {
      return 'OK'
    }
  } else {
    return filteredPromises.join('\n')
  }
}

app.get('/vp-status', (_, res) => {
  getStatus().then((status) => {
    res.send(status)
  }).catch(() => {
    res.send('was unable to query status')
  })
})

app.get('/vp-content', (_, res) => {
  res.json({
    institutions: elements.map((item) => ({
      id: item.id,
      title: item.title
    }))
  })
})

elements.forEach((element) => {
  app.use('/vp-institution/' + element.id, element.router)
})

app.listen(config.port)
