/*
 * vertretungsplan.io custom crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Router } from 'express'
import { wrapCrawler } from '../crawlutil.js'
import { InstitutionElement } from '../element.js'
import { crawl } from './crawler.js'

export function domgymVerdenElementCreator (): InstitutionElement {
  const router = Router()

  const crawlUtil = wrapCrawler((abort) => crawl(abort))

  router.get('/config/default', (_, res) => {
    res.json({
      config: [ /* no parameters */ ],
      configValidationConditionId: '_true',
      contentBucketSets: [
        {
          id: 'default',
          usageConditionId: '_true',
          type: 'content'
        }
      ],
      conditionSets: [ /* nothing */ ]
    })
  })

  router.get('/content/default', (_, res, next) => {
    crawlUtil.getLastSuccessPromise().then((data) => {
      res.json(data)
    }).catch((ex) => next(ex))
  })

  return {
    id: 'domgym-verden',
    title: 'Domgymnasium Verden',
    router,
    getStatus: () => crawlUtil.getLastPromise()
  }
}
